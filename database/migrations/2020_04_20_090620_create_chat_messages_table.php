<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChatMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chat_messages', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('chat_room_id')->unsigned();
            $table->bigInteger('participant_id')->unsigned();
            $table->string('message', 100);
            $table->boolean('is_delivered')->default(0);
            $table->boolean('is_read')->default(0);
            $table->timestamps();

            $table->foreign('chat_room_id')->references('id')->on('chat_rooms')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('participant_id')->references('id')->on('chat_participants')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chat_messages');
    }
}
