<template>
    <div class="row justify-content-center">
        <div class="col-md-4 col-xl-3 chat">
            <div class="card mb-sm-3 mb-md-0 contacts_card">
                <div class="card-header">
                    <div class="d-flex bd-highlight">
                        <div class="p-2 flex-grow-1 bd-highlight">
                            <div class="img_cont">
                                <img :src="auth_user.avatar" class="rounded-circle user_img">
                                <span class="online_icon"></span>
                            </div>
                        </div>
                        <div class="p-2 bd-highlight">
                            <span v-on:click="contactMenu('open')" class="btn"><i class="text-white fas fa-paper-plane fa-lg"></i></span>
                        </div>
                        <div class="p-2 bd-highlight">
                            <span @click="optionMenu(2)" id="action_menu_btn2"><i class="fas fa-ellipsis-v"></i></span>
                            <div class="action_menu action_menu2">
                                <ul>
                                    <li><i class="fas fa-users"></i> New Group</li>
                                    <li><i class="fas fa-user"></i> Profile</li>
                                    <li><i class="fas fa-cogs"></i> Setting</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="input-group">
                        <input type="text" placeholder="Search..." name="" class="form-control search">
                        <div class="input-group-prepend">
                            <span class="input-group-text search_btn"><i class="fas fa-search"></i></span>
                        </div>
                    </div>
                </div>
                <div class="card-body contacts_body">
                    <ul class="contacts">
                        <template v-for="(x,y) in orderedRooms">
                            <li :class="active_room == x.id ? 'active' : ''" :key="y">
                                <a href="#" v-on:click="setActiveRoom(x.id)">
                                    <div class="d-flex bd-highlight">
                                        <div class="img_cont">
                                            <img :src="x.avatar" class="rounded-circle user_img">
                                            <span class="online_icon"></span>
                                        </div>
                                        <div class="user_info">
                                            <span>{{ x.name }}</span>
                                            <p v-if="x.typing">{{ x.typing }}</p>
                                            <p v-else-if="x.last_chat">{{ x.last_chat.message }}</p>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        </template>
                    </ul>
                </div>
                <div class="card-footer"></div>
                <div class="card mb-sm-3 mb-md-0 contacts_card p-4" id="contact_overlay">
                    <div class="input-group mt-3 mb-2">
                        <input style="border: 1px solid!important" type="text" placeholder="Search..." name="" class="form-control search">
                        <div class="input-group-prepend">
                            <span style="border: 1px solid!important" class="input-group-text search_btn"><i class="fas fa-search"></i></span>
                        </div>
                    </div>
                    <ul class="contacts">
                        <template v-for="(x,y) in contacts">
                            <li :class="active_room == x.id ? 'active' : ''" :key="y">
                                <a href="#" v-on:click="newRoom(x.id)">
                                    <div class="d-flex bd-highlight">
                                        <div class="img_cont">
                                            <img :src="x.avatar" class="rounded-circle user_img">
                                            <span class="online_icon"></span>
                                        </div>
                                        <div class="user_info">
                                            <span>{{ x.name }}</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        </template>
                    </ul>
                    <span v-on:click="contactMenu('close')" id="contact_overlay_close">x</span>
                </div>
            </div>
        </div>
        <div class="col-md-8 col-xl-6 chat">
            <div class="card">
                <template v-if="active_room">
                    <div class="card-header msg_head">
                        <div class="d-flex bd-highlight">
                            <div class="img_cont">
                                <img :src="messages.avatar" class="rounded-circle user_img">
                                <span class="online_icon"></span>
                            </div>
                            <div class="user_info">
                                <span>{{ messages.name }}</span>
                                <p v-if="messages.typing == '' && messages.room_message">{{messages.room_message.length}} Pesan</p>
                                <p v-else>{{messages.typing}}</p>
                            </div>
                        </div>
                        <span @click="optionMenu(1)" id="action_menu_btn"><i class="fas fa-ellipsis-v"></i></span>
                        <div class="action_menu action_menu1">
                            <ul>
                                <li><i class="fas fa-ban"></i> End Chat</li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body msg_card_body" id="chats" v-if="messages">
                        <template v-for="(x, y) in messages.room_message">
                            <div class="d-flex justify-content-start mb-4" v-if="x.sender.user_id != auth_user.id" :key="y">
                                <div class="img_cont_msg">
                                    <img :src="messages.avatar" class="rounded-circle user_img_msg">
                                </div>
                                <div class="msg_cotainer">
                                    {{ x.message }}
                                    <span class="msg_time" style="min-width:80px">{{new Date(x.created_at) | dateFormat('H:mm') }}, Today</span>
                                </div>
                            </div>
                            <div class="d-flex justify-content-end mb-4" v-else :key="y">
                                <div class="msg_cotainer_send">
                                    {{ x.message }}
                                    <span class="msg_time_send text-right" style="min-width:80px">{{new Date(x.created_at) | dateFormat('H:mm') }}, Today
                                        <span v-if="x.status == 'Pending'"><i class="fas fa-check"></i></span>
                                        <span v-else-if="x.status == 'Delivered'"><i class="fas fa-check-double"></i></span>
                                        <span v-else-if="x.status == 'Read'"><i class="fas fa-check-double text-primary"></i></span>
                                    </span>
                                </div>
                                <div class="img_cont_msg">
                                    <img :src="auth_user.avatar" class="rounded-circle user_img_msg">
                                </div>
                            </div>
                        </template>
                    </div>
                    <div class="card-footer">
                        <div class="input-group">
                            <div class="input-group-append">
                                <span class="input-group-text attach_btn"><i class="fas fa-paperclip"></i></span>
                            </div>
                            <textarea v-model="message"  v-on:keyup.enter="sendMessage()" class="form-control type_msg" placeholder="Type your message..."></textarea>
                            <div class="input-group-append">
                                <span v-on:click="sendMessage()" class="input-group-text send_btn"><i class="fas fa-location-arrow"></i></span>
                            </div>
                        </div>
                    </div>
                </template>
                <template v-else>
                    <div>
                        <center class="text-white mt-5"><h2><strong>Sinau Realtime Chat</strong></h2></center>
                    </div>
                </template>
            </div>
        </div>
    </div>
</template>

<script>
    export default {
        props: ['auth_user', 'contacts'],
        data: function() {
            return {
                rooms : '',
                messages : {},
                message : '',
                div_chat : '',
                active_room : null,
                is_typing : false
            }
        },
        mounted() {
            this.fetchRoom();
            Echo.channel('new_room_'+this.auth_user.id)
                .listen('RoomChat', (e) => {
                    if(e.data.user_id != this.auth_user.id){
                        this.pushNewChatRoom(e.message)
                    }
                })
        },
        computed: {
            orderedRooms: function () {
                return _.orderBy(this.rooms, 'updated_at').reverse()
            }
        },
        methods: {
            sendMessage(){
                axios.post('/message', {
                        room_id:this.messages.id,
                        message:this.message
                })
                .then(function (response) {
                    this.pushNewMessage(response.data.data)
                }.bind(this))
                .catch(function (error) {
                    alert(error)
                })
                this.is_typing = true;
                this.message = '';
            },
            newRoom(id){
                axios.post('/chat', {
                        room_id:this.messages.id,
                        participants:[id]
                })
                .then(function (response) {
                    this.pushNewChatRoom(response.data.data)
                    this.contactMenu('close')
                }.bind(this))
                .catch(function (error) {
                    alert(error)
                })
            },
            broadcastTyping(room_id, user_id, message, type){
                axios.post('/typing', {
                        data : {
                            room_id: room_id,
                            user_id: user_id,
                            message: message,
                            type: type
                            }
                })
                .then(function (response) {
                }.bind(this))
                .catch(function (error) {
                    alert(error)
                })
            },
            pushNewMessage(message){
                if(message.chat_room_id == this.active_room){
                    this.messages.room_message.push(message)
                }
                var index_message = this.rooms.findIndex(x => x.id == message.chat_room_id)
                    this.rooms[index_message].last_chat = message
                    this.rooms[index_message].updated_at = message.created_at
            },
            pushNewChatRoom(room){
                this.rooms.push(room)
            },
            pushTypingMessage(message){
                var index_room = this.rooms.findIndex(x => x.id == message.room_id)
                if(message.type == 'start'){
                    if(message.room_id == this.active_room){
                        this.messages.typing = 'Sedang Mengetik  ...'
                    }
                    this.rooms[index_room].typing = 'Sedang Mengetik  ...'
                }else{
                    if(message.room_id == this.active_room){
                        this.messages.typing = ''
                    }
                    this.rooms[index_room].typing = ''
                }
            },
            scrollToBottom(){
                this.div_chat = document.getElementById("chats")
                this.div_chat.scrollTop = this.div_chat.scrollHeight;
            },
            updateMessageStatus(id, status){
                axios.patch('/message/'+id, {
                        type:status
                })
                .then(function (response) {
                    var index_message = this.messages.room_message.findIndex(x => x.id == id)
                    this.messages.room_message[index_message].is_read = 1
                    this.messages.room_message[index_message].status = 'Read'
                }.bind(this))
                .catch(function (error) {
                    alert(error)
                })
            },
            setActiveRoom(id){
                this.active_room = id
            },
            fetchRoom(){
                axios.get("/chat", {
                    params: {
                        type:'json'
                    }
                })
                .then(function (response) {
                    this.rooms = response.data.data
                    this.rooms.forEach(element => {
                        Echo.channel('message_'+element.id)
                            .listen('NewMessage', (e) => {
                                if(e.message.sender.user_id != this.auth_user.id){
                                    this.pushNewMessage(e.message)
                                }
                            })
                        Echo.channel('typing_'+element.id)
                            .listen('TypingMessage', (e) => {
                                if(e.data.user_id != this.auth_user.id){
                                    this.pushTypingMessage(e.data)
                                }
                            })
                    })
                }.bind(this))
                .catch(function (error) {
                })
            },
            fetchMessage(room_id){
                axios.get("/message", {
                    params: {
                        room_id:room_id
                    }
                })
                .then(function (response) {
                    this.messages = response.data.data
                }.bind(this))
                .catch(function (error) {
                })
            },
            listenMessageStatus(){
                Echo.channel('message_status_'+this.messages.id)
                    .listen('MessageStatus', (e) => {
                        var index_message = this.messages.room_message.findIndex(x => x.id == e.message.id)
                        this.messages.room_message[index_message].status = e.message.status
                    })
            },
            optionMenu(v){
                if(v == 1){
                    $('.action_menu1').toggle();
                }else{
                    $('.action_menu2').toggle();
                }
            },
            contactMenu(type){
                if(type == 'open'){
                    document.getElementById("contact_overlay").style.right = 0;
                    document.getElementById("contact_overlay").style.left = 0;
                }else{
                    document.getElementById("contact_overlay").style.right = '200%';
                    document.getElementById("contact_overlay").style.left = '-100%';
                }
            }
        },
        watch: {
            messages: {
                handler(val){
                    this.messages.room_message.forEach(element => {
                        if(element.sender.user_id != this.auth_user.id){
                            if(element.status != 'Read'){
                                this.updateMessageStatus(element.id, 'read');
                            }
                        }else{
                            if(element.status != 'Read'){
                                this.listenMessageStatus();
                            }
                        }
                    });
                    this.scrollToBottom();
                },
                deep: true
            },
            active_room: function() {
                this.fetchMessage(this.active_room);
            },
            message: function() {
                if(this.message != '' && this.is_typing == false){
                    this.broadcastTyping(this.active_room , this.auth_user.id, this.auth_user.name, 'start')
                    this.is_typing = true;
                }else if(this.message == '' && this.is_typing == true){
                    this.broadcastTyping(this.active_room , this.auth_user.id, this.auth_user.name, 'stop')
                    this.is_typing = false;
                }
            }
        }
    }
</script>
