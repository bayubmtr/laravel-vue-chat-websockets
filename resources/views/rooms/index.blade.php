@extends('layouts.app')

@section('content')
<div class="container-fluid h-100">
    <chat :auth_user="{{ auth()->user() }}" :contacts="{{$contacts}}"></chat>
</div>
@endsection
