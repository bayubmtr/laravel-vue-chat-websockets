@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                Create New Room
                </div>
                <div class="card-body">
                    <form action="{{ route('room.store') }}" method="POST">
                    @csrf
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label">Name</label>
                            <div class="col-sm-10">
                            <input type="text" name="name" class="form-control-plaintext" id="staticEmail" placeholder="Room Name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-2 col-form-label">User</label>
                            <div class="col-sm-10">
                                <select name="participants[]" class="form-control">
                                    @foreach($user as $var)
                                    <option value="{{ $var->id }}">{{ $var->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <input type="submit" class="btn btn-outline-primary" value="create">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
