<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ChatRoom;
use App\ChatParticipant;

class RoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = ChatRoom::with('last_chat', 'room_participant.user')->whereHas('room_participant', function($q){
            $q->where('user_id', auth()->user()->id);
        })->orderBy('updated_at', 'desc')->get();
        $data->each(function($q){
            if($q->room_type == 'single'){
                $participant = $q->room_participant->where('user_id', '!=', auth()->user()->id)->first();
                $q->name = $participant['user']['name'];
                $q->avatar = $participant['user']['avatar'];
                $q->typing = '';
            }
        });
        if($request->type == 'json'){
            return response()->json(['code' => 200, "message" => "Success Get Rooms !", "data" => $data], 200);
        }
        $contacts = \App\User::where('id', '!=', auth()->user()->id)->get();
        return View('rooms.index', compact('data', 'contacts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = \App\User::get();

        return View('rooms.create', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $room_name = $request->name ? $request->name : auth()->user()->id.','.$request->participants[0];
        $data = ChatRoom::firstOrCreate([
            "name" => $room_name,
            "owner_id" => auth()->user()->id,
            "room_type" => $request->room_type ? $request->room_type : 'single'
        ]);

        if($data){
            $participants = ChatParticipant::forceCreate([
                "chat_room_id" => $data->id,
                "user_id" => auth()->user()->id
            ]);
    
            foreach ($request->participants as $key => $value) {
                $participants = ChatParticipant::forceCreate([
                    "chat_room_id" => $data->id,
                    "user_id" => $value
                ]);
            }
            if($data->room_type == 'single'){
                $participant = $data->room_participant->where('user_id', '!=', auth()->user()->id)->first();
                $data->name = $participant['user']['name'];
                $data->avatar = $participant['user']['avatar'];
            }
        }

        return response()->json(['code' => 200, "message" => "Success Get Message !", "data" => $data], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = ChatRoom::findOrFail($id);
        $data->delete();
        return response()->json(['code' => 200, "message" => "Success Delete Message !", "data" => $data], 200);
    }
}
