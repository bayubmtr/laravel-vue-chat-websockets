<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ChatMessage;
use App\ChatParticipant;
use App\ChatRoom;
use App\Events\NewMessage;
use App\Events\MessageStatus;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = ChatRoom::with('room_participant.user', 'room_message.sender.user')->where('id', $request->room_id)->first();
        if($data->room_type == 'single'){
            $participant = $data->room_participant->where('user_id', '!=', auth()->user()->id)->first();
            $data->name = $participant['user']['name'];
            $data->avatar = $participant['user']['avatar'];
            $data->typing = '';
        }
        return response()->json(['code' => 200, "message" => "Success Get Message !", "data" => $data], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $room = ChatRoom::find($request->room_id);
        if(!$room){
            return response()->json(['code' => 404, "message" => "Room Not Found !"], 404);
        }
        $room->touch();
        $sender = ChatParticipant::where('user_id', auth()->user()->id)->where('chat_room_id', $request->room_id)->first();
        if(!$sender){
            return response()->json(['code' => 403, "message" => "You Cant Send Message In This Room !"], 403);
        }
        $data = ChatMessage::forceCreate([
            "chat_room_id" => $request->room_id,
            "participant_id" => $sender->id,
            "message" => $request->message,
            "is_delivered" => 0,
            "is_read" => 0
        ]);
        $data->sender->user;
        event(new NewMessage('message_'.$data->chat_room_id, $data));
        
        return response()->json(['code' => 200, "message" => "Success Send Message !", "data" => $data], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = ChatMessage::findOrFail($id);

        return View('rooms.message.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = ChatMessage::with('sender.user')->where('id', $id)->first();
        if(!$data){
            return response()->json(['code' => 404, "message" => "Message Not Found !"], 404);
        }
        $type = $request->type;
        if($type == 'delivered'){
            $data->is_delivered = 1;
            $data->save();
            event(new MessageStatus('message_status_'.$data->chat_room_id, $data));
        }elseif($type == 'read'){
            $data->is_read = 1;
            $data->save();
            event(new MessageStatus('message_status_'.$data->chat_room_id, $data));
        }
        return response()->json(['code' => 200, "message" => "Success Update Message !"], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = ChatMessage::find($id);
        if(!$data){
            return response()->json(['code' => 404, "message" => "Message Not Found !"], 404);
        }
        $data->delete();
        return response()->json(['code' => 200, "message" => "Success Deleted Message !"], 200);
    }
}
