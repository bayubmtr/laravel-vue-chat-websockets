<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatRoom extends Model
{
    protected $fillable = [
        'owner_id', 'name', 'room_type'
    ];
    //
    public function room_participant(){
        return $this->hasMany('App\ChatParticipant', 'chat_room_id', 'id');
    }
    public function owner(){
        return $this->belongsTo('App\User', 'owner_id', 'id');
    }
    public function room_message(){
        return $this->hasMany('App\ChatMessage', 'chat_room_id', 'id');
    }
    public function last_chat(){
        return $this->hasOne('App\ChatMessage', 'chat_room_id', 'id')->latest();
    }
}
