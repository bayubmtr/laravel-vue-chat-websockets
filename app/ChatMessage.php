<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatMessage extends Model
{
    //
    protected $appends = ['status'];
    protected $casts = [
        'properties' => 'array',
    ];
    public function room(){
        return $this->belongsTo('App\ChatRoom', 'chat_room_id', 'id');
    }
    public function sender(){
        return $this->belongsTo('App\ChatParticipant', 'participant_id', 'id');
    }
    public function getStatusAttribute()
    {
        if($this->is_read == 1){
            return "Read";
        }elseif($this->is_delivered == 0){
            return "Pending";
        }elseif($this->is_delivered == 1){
            return "Delivered";
        }
    }
}
